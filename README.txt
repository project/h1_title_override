H1 Title Override
-----------------

This module lavareges the Context module to set or override H1 page titles per path or 
other context contitions. The module provides a context reaction and supports tokens.

Note:
If you do not see a "H1 Title" reaction in the reactions dropdown after installing 
this module, clear all cache.